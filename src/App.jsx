import { Route, Routes } from 'react-router-dom';
import { Menu, ConsultaForm, RegistroForm, ResultadoCuenta } from "./components";
import { getCliente } from './clienteReducer';
import { useSelector } from 'react-redux';
import { getCuenta } from './cuentaReducer';


function App() { 
  var clienteFields = [
    {name: 'Paterno', type: 'text'},
    {name: 'Materno', type: 'text'},
    {name: 'Nombres', type: 'text'},
    {name: 'CI', type: 'text'},
    {name: 'Fecha de Nacimiento', type: 'date'},
    {name: 'Genero', type: 'text'},
    {name: 'Celular', type: 'text'},
    {name: 'Nivel de Ingresos', type: 'text'},
    {name: 'Correo', type: 'email'},
  ]
  var cuentaFields = [
    {name: 'CI', type: 'text'},
    {name: 'Nombre', type: 'text'},
    {name: 'Cuenta', type: 'text'}
  ]
  return (
    <div className="App" data-testid="App">
      <Routes>
        <Route path="/" element={<Menu />} />
        <Route path="/ConsultaCliente" element={<ConsultaForm consulta="CI: " />} />
        <Route path="/ConsultaCuenta" element={<ConsultaForm consulta="Cuenta: " />} />
        <Route path="/RegistroCliente" element={<RegistroForm fields={clienteFields}  type={"cliente"}/>} />
        <Route path="/RegistroCuenta" element={<RegistroForm fields={cuentaFields} type={"cuenta"}/>} />
        <Route path="/ResultadoCliente" element={<ResultadoCuenta type={"cliente"} />} />
        <Route path="/ResultadoCuenta" element={<ResultadoCuenta type={"cuenta"}/>} />
      </Routes>
    </div>
  );
}

export default App;