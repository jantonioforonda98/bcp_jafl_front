import { Header } from '../Header/Header';
import style from './Menu.module.css';
import {useNavigate} from 'react-router-dom';

export function Menu () {
  const navigate = useNavigate();
  return (
    <div className={style.container}>
      <Header />
      <div className={style.actions}>
        <button className={style.button} onClick={() => {navigate('/ConsultaCliente');}}>Consulta Cliente</button>
        <button className={style.button} onClick={() => {navigate('/ConsultaCuenta');}}>Consulta Cuenta</button>
        <button className={style.button} onClick={() => {navigate('/RegistroCliente');}}>Registro Cliente</button>
        <button className={style.button} onClick={() => {navigate('/RegistroCuenta');}}>Registro Cuenta</button>
      </div>
    </div>


  );
}