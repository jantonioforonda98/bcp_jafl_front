import style from './Header.module.css';
import logo from '../../utils/logo.png';

export function Header () {
  return (
    <div className={style.div}>
      <img className={style.image} src={logo}/>
      <h1 className={style.h1}>Administracion de cuentas y clientes</h1>
    </div>
  );
}