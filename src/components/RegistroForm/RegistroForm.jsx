import style from './RegistroForm.module.css';
import { Header } from '../Header/Header';
import { useDispatch, useSelector } from 'react-redux';
import { getCliente, postCliente } from '../../clienteReducer';
import { getCuenta, postCuenta } from '../../cuentaReducer';

export function RegistroForm ({fields, type}) {
  const dispatch = useDispatch();
  const select = useSelector(getCliente);
  const selectedCuenta = useSelector(getCuenta);
  const cliente = {};
  const handleChange = (name, value) => {
    name = name.toLowerCase();
    if (name == "fecha de nacimiento"){
      name = "fechaDeNacimiento";
    };
    if (name == "nivel de ingresos"){
      name = "nivelDeIngresos";
    }
    cliente[name.toLowerCase()] = value;
  }
  return (
    <div className={style.container}>
      <Header />
      <div className={style.actions}>
        {fields?.map((field) => {
          return (
            <form id = "form" className={style.field} key={field?.name}>
              <label className={style.label}>{field?.name}: </label>
              <input className={style.input}
                type={field?.type}
                disabled={field?.disabled}
                onChange={(e) => {handleChange(field?.name, e.target.value)}}
                required = {true}/>
            </form>
          );
        })}
        <button
          className={style.button}
          onClick={() =>{
            cliente['fechaDeRegistro'] = new Date().toJSON();
            cliente['fechaDeActualizacion'] = new Date().toJSON();
            if (type == "cliente"){
              dispatch(postCliente(cliente));
              const name = select?.paterno
              if (name?.startsWith("Error") || name == undefined){
                window.alert("An Error has occured:  " + name);
              }
              else{
                window.alert("Cliente creado");
              };
            }
            else{
              dispatch(postCuenta(cliente));
              const name = selectedCuenta?.cuenta;
              if (name?.startsWith("Error") || name == undefined){
                window.alert("An Error has occured: " + name);
              }
              else{
                window.alert("Cuenta creada");
              }
            }
            var form = document.getElementById("form");
            form.reset();
          }}>Enviar</button>
      </div>
    </div>
  );
};