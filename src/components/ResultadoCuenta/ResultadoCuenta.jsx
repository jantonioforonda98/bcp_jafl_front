import style from './ResultadoCuenta.module.css';
import { Header } from '../Header/Header';
import { useSelector } from 'react-redux';
import { getCliente } from '../../clienteReducer';
import {useNavigate} from 'react-router-dom';
import { getCuenta } from '../../cuentaReducer';

export function ResultadoCuenta ({type}) {
  const navigate = useNavigate();
  const cliente = useSelector(getCliente);
  const cuenta = useSelector(getCuenta);
  console.log(cliente);
  return (
    <div className={style.container}>
      <Header />
      <div className={style.actions}>
        {Object.keys(type == "cliente" ? cliente : cuenta).map((key, index) => {
          return (
            <div className={style.field} key={index}>
              <label className={style.label}>{key.replace(/([A-Z])/g, " $1").charAt(0).toUpperCase() + key.replace(/([A-Z])/g, " $1").slice(1)}: </label>
              <input className={style.input}
                disabled={true}
                value={cliente[key]}
              />
            </div>
          );
        })}
        <button className={style.button} onClick={() => {navigate('/ConsultaCliente');}}>Volver</button>
      </div>
    </div>
  );
};