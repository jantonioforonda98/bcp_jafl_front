import { Menu } from "./Menu/Menu"
import { Header } from "./Header/Header"
import { ConsultaForm } from "./ConsultaForm/ConsultaForm"
import { RegistroForm } from "./RegistroForm/RegistroForm"
import { ResultadoCuenta } from "./ResultadoCuenta/ResultadoCuenta"

export {
  ConsultaForm,
  Header,
  Menu,
  RegistroForm,
  ResultadoCuenta
}