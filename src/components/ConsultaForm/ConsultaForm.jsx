import style from './ConsultaForm.module.css';
import { Header } from '../Header/Header';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCliente, getCliente, resetCliente } from '../../clienteReducer';
import {useNavigate} from 'react-router-dom';
import { fetchCuenta, getCuenta, resetCuenta } from '../../cuentaReducer';

export function ConsultaForm ({consulta}) {
  const cliente = useSelector(getCliente);
  const cuenta = useSelector(getCuenta);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [value, setValue] = useState('');
  return (
    <div className={style.container}>
      <Header />
      <div className={style.actions}>
        <label className={style.label}>{consulta}</label>
        <input className={style.input} type="text" value={value} onChange={(e) => {setValue(e.target.value)}}></input>
        <button className={style.button}
        onClick={() => {
          if (consulta == "CI: "){
            dispatch(resetCliente());
            dispatch(fetchCliente(value));
            if (cliente?.materno != null){
              navigate("/ResultadoCliente");
            }
            if (cliente?.materno == null){
              window.alert("No se encontro al cliente. Intente mas tarde");
            }
          }
          else{
            dispatch(resetCuenta());
            dispatch(fetchCuenta(value));
            if (cuenta?.ci != null){
              navigate("/ResultadoCuenta");
            }
            if (cuenta?.ci == null){
              window.alert("No se encontro la cuenta. Intente mas tarde");
            }

          }
        }}>Enviar</button>
      </div>
    </div>
  );
};