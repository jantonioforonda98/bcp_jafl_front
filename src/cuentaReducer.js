import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchCuenta = createAsyncThunk('user/fetchCuenta', async (payload) => {
  return await fetch('https://localhost:7141/Cuenta?cuenta=' + payload, {method: 'GET'})
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    return data;
  });
});
export const postCuenta = createAsyncThunk('user/postCuenta', async (requestBody) => {
  try {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(requestBody)
    };
    return await fetch('https://localhost:7141/Cuenta', requestOptions)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      return data;
    });
  } catch (error) {
    return error;
  }
});

const cuentaSlice = createSlice({
  name: "Cuenta",
  initialState: {
    cuenta: {},
    loading: false,
    error: ''
  },
  reducers: {
    resetCuenta: (state) => {
      state.cuenta.cuenta = {};
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchCuenta.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchCuenta.fulfilled, (state, action) => {
        state.loading = false;
        state.cuenta = action.payload;
      })
      .addCase(fetchCuenta.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(postCuenta.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(postCuenta.fulfilled, (state, action) => {
        state.loading = false;
        state.cuenta = action.payload;
      })
      .addCase(postCuenta.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const getCuenta = (state) => state.cuenta.cuenta;
export const { resetCuenta } = cuentaSlice.actions;

export default cuentaSlice.reducer;
