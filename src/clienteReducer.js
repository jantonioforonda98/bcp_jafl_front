import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchCliente = createAsyncThunk('user/fetchCliente', async (payload) => {
  return await fetch('https://localhost:7141/Cliente?ci=' + payload, {method: 'GET'})
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    return data;
  });
});
export const postCliente = createAsyncThunk('user/postCliente', async (requestBody) => {
  try {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(requestBody)
    };
    return await fetch('https://localhost:7141/Cliente', requestOptions)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      return data;
    });
  } catch (error) {
    return error;
  }
});

const clienteSlice = createSlice({
  name: "Cliente",
  initialState: {
    cliente: {},
    loading: false,
    error: ''
  },
  reducers: {
    resetCliente: (state) => {
      state.cliente.cliente = {};
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchCliente.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchCliente.fulfilled, (state, action) => {
        state.loading = false;
        state.cliente = action.payload;
      })
      .addCase(fetchCliente.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(postCliente.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(postCliente.fulfilled, (state, action) => {
        state.loading = false;
        state.cliente = action.payload;
      })
      .addCase(postCliente.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const getCliente = (state) => state.cliente.cliente;
export const { resetCliente } = clienteSlice.actions;

export default clienteSlice.reducer;
