import { combineReducers } from "redux";

import clienteReducer from './clienteReducer.js';
import cuentaReducer from "./cuentaReducer.js";

export const rootReducer = combineReducers({
    cliente: clienteReducer,
    cuenta: cuentaReducer,
})
